# MongoDB Realm VS Firebase 

**Ici je vais citée quelque point fort & faible entre Firebase et MongoDB Realm suites au recherches effectuer**


## Les point fort  
| MongoDB Realm | Native data layers |
| ------:| -----------:|
| `Facile a utilisée` | QUERY LANGAGE : creation et utilisation de Shema facile a entreprendre |
| `Api elegant`    | Documentations et code facile a lire et reproduire|
| `syncronisasion Cloud`    | Synchonisation & modification sur le cloud Rapide avec REALM SYNC "" |
| `React native support`    |  Support pour des probleme liée a l'integration ou l'utilisation AVEC RN|
| `Open Source`    | Communauté de developper evolutive |



| Firebase | Realtime Cloud data monitoring |
| ------:| -----------:|
| `Facile a utilisée`  | La logique de construction et utilisée sur le Client et les DATARULES dans un fichier JSON  |
| `Push notification` | Plus de controle sur le workflow |
| `Bonne documentation`    |  Communautée active |
| `Securité`    | Travaille avec les gateaways & Fais par google |
| `Adaptation a IOS`    |  Est adaptée au format IOS|



## Les point faible 
| MongoDB Realm | Native data layers |
| ------:| -----------:|
| `Aucun reconnu pour l'instant`  | Car peux de communauté ainsi que d'entreprise utilisant cette techno |



| Firebase | Cloud data monitoring |
| ------:| -----------:|
| `No open-source`  | Vous depender dela company : externe |
| `Scalability` | Pas infini |
| `Filtrage Query`    | Nous ne pouvons pas filtrer les query|
| `Prix`    | Peux revenir chers dependant les donnée sotcker de l'app |
| `Server Connection problem`    | Plusieur issues concernant des probleme de connexion liée au serveur de firebase |


## MASS CHECKING 

<p align="center" display="flex">
  <img src="./assets/RealmMass.png" width="350" title="Npm Anvaka Realm">
  <img src="./assets/MongooseMass.png" width="350" title="Npm Anvaka mongoose">
  <img src="./assets/firebaseMass.png" width="350" alt="Npm Anvaka Firebase">
</p>


**Voici mon analyze par rapport a l'utilisation de ses techno**

## Instalation
**Mongoose** : npm i mongoose (... Packets)
**Firebase** : npm i firebase (... Packets)
## DataSets
**Mongoose** : 
**Firebase** :
## Mutation
**Mongoose** :
**Firebase** :
## Price 
**Mongoose** :
**Firebase** :


La techno que je choisirais pour un projet scalable comme Scribbe serais realm car 
* Le prix depend de requete recu ...
* il n'y as pas de probleme de connexion sur le serveur 
* Query langage tres facile a prendre en main 
* Typage


## Documentation 
https://stackoverflow.com/questions/63215290/can-you-use-realm-sync-with-an-m0-tier-mongodb-cluster ** (A Voir )**
https://docs.mongodb.com/realm/billing/ ** A Lire (prix data realm)**

https://firebasetutorials.com/is-firebase-hosting-free/
https://satvasolutions.com/what-is-firebase-and-how-to-setup-for-web-and-mobile-based-apps/
https://medium.com/wesionary-team/why-firebase-6cd7739d1960
https://stackshare.io/stackups/firebase-vs-realm
https://crisp.chat/blog/why-you-should-never-use-firebase-realtime-database/




Fais par [Weertz.Joffrey](https://github.com/jSUNSH1NEw "jSUNSH1NEw")
