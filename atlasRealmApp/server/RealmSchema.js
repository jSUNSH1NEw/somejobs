SyncConfigurationBase.Initialize(UserPersistenceMode.NotEncrypted);

let authUrl = new Uri("https://MY-REALM-CLOUD-NAME.cloud.realm.io");
let credentials = Credentials.UsernamePassword("MY-USERNAME", "MY-PASSWORD", createUser: false);
let user = await User.LoginAsync(credentials, authUrl);

let realmUrl = new Uri("realms://MY-REALM-CLOUD-NAME.cloud.realm.io/~/myRealm");

let configuration = new FullSyncConfiguration(realmUrl,user: user);


//Realm realm = Realm.GetInstance(new RealmConfiguration(Environment.CurrentDirectory+"/database.realm"));
Realm realm = await Realm.GetInstanceAsync();
realm.Write(() =>
                {
                    realm.Add(new Dog { Name = "RuffRiders", Age = 111});
            	});

                let oldDogs = realm.All<Dog>().Where(dog => dog.Age > 1);
                foreach (let dog in oldDogs)
                {
                    Console.WriteLine(dog.Name);
                }
		
Realm.Sync.setLogLevel("debug");






